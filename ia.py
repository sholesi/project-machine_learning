import seaborn as sns
from sklearn import datasets
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.naive_bayes import GaussianNB
import matplotlib as mpl
import numpy as np

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
# See PyCharm help at https://www.jetbrains.com/help/pycharm/



iris = datasets.load_iris()
#print(iris.target)
#print(iris)
#print(iris.feature_names)
#print(iris.data[2:5])
#5 nn inclus (de 0 a 5 colonnes)
data=iris.data
#print(data[0:2,0:1])
#print(data[:3])
#nb de lignes et nb de colonnes d'abord sachant qu'on exclut la 2e valeur format = [nbL,nbC]

# print(data.shape)
# print(iris.target_names)
# target=iris.target
# print(target.shape)

fig=plt.figure(figsize=(8,4))
fig.subplots_adjust(hspace=0.4,wspace=0.4)
ax1=plt.subplot(1,2,1)
#subplot=divise le cadre en plusieurs parties (2e para) 1e = divisions y et 3e= emplacement sous x

clist =['violet','yellow','blue']
colors=[clist[c] for c in iris.target]

#print(data[:,0])

ax1.scatter(data[:,0],data[:,1],c=colors)
plt.xlabel("long sepal")
plt.ylabel("larg sepal")

ax2=plt.subplot(1,2,2)

ax2.scatter(data[:,2],data[:,3],c=colors)
plt.xlabel("long petal")
plt.ylabel("larg petal")

#for i in range(len(iris.target_names)):

# Légende
for ind, s in enumerate(iris.target_names):
    # on dessine de faux points, car la légende n'affiche que les points ayant un label
    plt.scatter([], [], label=s, color=clist[ind])
plt.legend()
plt.show()

clf=GaussianNB()

#print(data[:100])
#print(iris.data.shape)
#colonne

clf.fit(data[70:,:3],iris.target[70:])
#clf.fit(data[:,:2],iris.target)
#fit = apprentissage av le nom de fleurs et les données target = permet de connaitre
#le nom

resultat = clf.predict(data[:70,:3])
#resultat = clf.predict(data[2:,:])
#print(data[:20,:3])
print(resultat)
#print(iris.target[:20])

#print(resultat-iris.target)
#print(sum(resultat!=iris.target[100:]))

print(accuracy_score(resultat,iris.target[:70]))
#print(accuracy_score(resultat,iris.target))
#print(confusion_matrix(iris.target[110:],resultat))

#plus il y a des valeurs plus l'apprentissage sera bon au moins
#une centaine de valeurs

#x_min, x_max = data[:,]
x_min,x_max=data[20:,0].min()-1, data[20:,0].max()+1
y_min,y_max=data[20:,1].min()-1, data[20:,1].max()+1
z_min,z_max=data[20:,2].min()-1, data[20:,2].max()+1

x=np.arange(x_min,x_max,0.30)
y=np.arange(y_min,y_max,0.30)
z=np.arange(z_min,z_max,0.30)

#print(x)
#xx,yy=np.meshgrid(x,y)
xx,yy,zz=np.meshgrid(x,y,z)
#mettre ça dans une matrice
#print(list(zip(xx.ravel(),yy.ravel(),zz.ravel())))
data_samples=list(zip(xx.ravel(),yy.ravel(),zz.ravel()))
#applatit sur une ligne + creation de liste(x,y)

Z=clf.predict(data_samples)
#colors=['violet', 'yellow', 'red']
C=[clist[x] for x in Z]

ax=plt.axes(projection='3d')

print(yy.ravel().shape)
ax.scatter3D(xx.ravel(), yy.ravel(),zz.ravel(),c=C)
#plt.xlim(xx.min() - .1, xx.max() + .1)
#plt.ylim(yy.min() - .1, yy.max() + .1)
#plt.zlim(zz.min() - .1, zz.max() + .1)

for ind, s in enumerate(iris.target_names):
    # on dessine de faux points, car la légende n'affiche que les points ayant un label
    ax.scatter([], [], label=s, color=clist[ind])
ax.legend()
plt.show()

from sklearn.decomposition import PCA
model= PCA(n_components=2)
model.fit(iris.data)
#reduc=prediction
reduc=model.transform(iris.data)
print(reduc.shape)
plt.scatter(reduc[:,0],reduc[:,1],c=colors)
plt.xlabel('PCA1')
plt.xlabel('PCA2')
plt.show()

#regroupement
from sklearn.mixture import GaussianMixture
model=GaussianMixture(n_components=3,covariance_type='full')
model.fit(reduc[:,:2])
groups=model.predict(reduc[:,:2])

#print(groups)

#sns.lmplot("PCA1", "PCA2", data=reduc[:,:2], hue='label', fit_reg=False)




